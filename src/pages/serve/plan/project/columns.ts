export const columnLabel = [
  {
    colKey: 'rowIndex',
    title: '序号',
    width: '100'
  },
  {
    colKey: 'image',
    title: '护理图片',
    width: '100'
  },
  {
    colKey: 'name',
    title: '护理项目名称'
  },
  {
    colKey: 'price',
    title: '价格',
    ellipsis: true
  },
  {
    colKey: 'unit',
    title: '单位'
  },
  {
    colKey: 'orderNo',
    title: '排序'
  },
  {
    colKey: 'creator',
    title: '创建人'
  },
  {
    colKey: 'createTime',
    title: '创建时间'
  },
  {
    colKey: 'status',
    title: '状态'
  },
  {
    colKey: 'op',
    title: '操作'
  }
]
