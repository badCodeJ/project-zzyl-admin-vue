import { request } from '@/utils/request'
import type {
  FormLevel,
  ListResult,
  PlanListResult,
  ListModel,
  ProjecListModel,
  PlanListModel,
  ListArrangeResult
} from '@/api/model/serveModel'

// 获取护理等级所有数据
export function getLevelAllList(params) {
  return request.get<ListResult>({
    url: '/nursing/level/listAll',
    params
  })
}

// 获取等级详情
export function getLevelDetails(id) {
  return request.get<ListModel>({
    url: `/nursing/level/${id}`
  })
}

// 护理等级禁用启用
export function levelStatus(params) {
  return request.put({
    url: `/nursing/level/${params.id}/status/${params.status}`
  })
}

// 护理项目
// 所有护理项目信息
export function getAllProjectList() {
  return request.get<ProjecListModel>({
    url: `/nursing/project/all`
  })
}
// 分页查询护理项目信息
export function getProjectList(params) {
  return request.get<ProjecListModel>({
    url: `/nursing/project`,
    params
  })
}

// 护理任务
// 护理任务列表
export function getArrangeList(params) {
  return request.get<ListArrangeResult>({
    url: `/nursing/task/page`,
    params
  })
}
// 执行护理任务
export function executePlan(params) {
  return request.put({
    url: `/nursing/task/do?estimatedServerTime=${params.estimatedServerTime}&mark=${params.mark}&taskId=${params.taskId}&taskImage=${params.taskImage}`
  })
}
// 取消护理任务
export function cancelPlan(params) {
  return request.put({
    url: `/nursing/task/cancel?reason=${params.reason}&taskId=${params.taskId}`
  })
}
// 修改护理任务的执行日期
export function changePlanTime(params) {
  return request.put({
    url: `/nursing/task/updateTime?estimatedServerTime=${params.estimatedServerTime}&taskId=${params.taskId}`
  })
}
// 查看护理任务详情
export function getTaskDetail(params) {
  return request.get<ListArrangeResult>({
    url: `/nursing/task`,
    params
  })
}
// 给老人设置护理员
export function setNurseForOlder(params: any) {
  return request.put<PlanListModel>({
    url: `/elder/setNursing`,
    data: params
  })
}
